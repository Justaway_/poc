import { NextPage, GetServerSideProps } from "next";
import { useEffect } from "react";

import { Data } from "./api/hello";
import { getRandomUser } from "../lib/helper/getRandomUser";

interface FetchSSGProps {
  user?: Data;
}

const FetchSSG: NextPage<FetchSSGProps> = ({ user }) => {
  console.log("user", user);

  useEffect(() => {
    console.log("user effect :>> ", user);
  }, [user]);

  return (
    <div>
      <h1>SSR</h1>
      {user?.name}
    </div>
  );
};

export default FetchSSG;

export const getServerSideProps: GetServerSideProps<
  FetchSSGProps
> = async () => {
  const user = getRandomUser();

  return {
    props: {
      user,
    },
  };
};
