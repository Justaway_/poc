import { NextPage } from "next";
import Image from "next/image";

interface Props {}

const ImageTest: NextPage<Props> = () => {
  return (
    <div>
      <h1>Image Test</h1>
      <Image src="/images/shark.jpg" alt="shark" height={144} width={144} />
      <Image src="/images/dota2.jpg" alt="dota2" height={1080} width={1920} />
    </div>
  );
};

export default ImageTest;
