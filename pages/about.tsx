import type { NextPage, GetStaticProps } from "next";
import Head from "next/head";
import Image from "next/image";
import Layout from "./layout";

import { Data } from "./api/hello";

interface IAboutProps {
  user: Data;
}

const About: NextPage<IAboutProps> = ({ user }) => {
  return (
    <Layout>
      <Head>
        <title>About</title>
      </Head>

      <main>
        <h1>About</h1>
        <Image src="/images/shark.jpg" alt="shark" height={144} width={144} />

        <div>{user.name}</div>
      </main>
    </Layout>
  );
};

export default About;

export const getStaticProps: GetStaticProps<IAboutProps> = async () => {
  // ไม่ควรเรียก fetch จาก Api Routes
  // เพราะสามารถเรียกใช้ helper function ที่ดึง user ออกมาเลยได้
  // และ next จะไม่เอาตรงนี้ไป bundle ตอน build (เลยเป็น server side)
  // const res = await fetch("http://localhost:3000/api/hello");
  // const user = (await res.json()) as Data;

  return {
    props: {
      user: {
        name: "pp"
      },
    },
  };
};
