import React from "react";
import { NextPage } from "next";

interface ILayoutProps {
  children: React.ReactNode;
}

const Layout: NextPage<ILayoutProps> = ({ children }) => {
  return <div>{children}</div>;
};

export default Layout;
