import { NextPage, GetStaticProps } from "next";
import { useEffect } from "react";

import { Data } from "./api/hello";
import { getRandomUser } from "../lib/helper/getRandomUser";

interface FetchSSGProps {
  user?: Data;
}

const FetchSSG: NextPage<FetchSSGProps> = ({ user }) => {
  console.log("user", user);

  useEffect(() => {
    console.log("user effect :>> ", user);
  }, [user]);

  return (
    <div>
      <h1>SSG</h1>
      {user?.name}
    </div>
  );
};

export default FetchSSG;

export const getStaticProps: GetStaticProps<FetchSSGProps> = async () => {
  const user = getRandomUser()

  return {
    props: {
      user,
    },
  };
};
