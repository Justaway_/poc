import React from "react";
import { NextPage, GetStaticPaths, GetStaticProps } from "next";

interface IsrProps {
  id?: string;
  number: number;
}

const IsrTest: NextPage<IsrProps> = ({ id, number }) => {
  return (
    <div>
      <h1>IsrTest {id}</h1>
      <h2>{number}</h2>
    </div>
  );
};

export default IsrTest;

export const getStaticProps: GetStaticProps<IsrProps> = ({ params }) => {
  const randomNumber = Math.floor(Math.random() * 10);

  return {
    props: {
      number: randomNumber,
      id: params?.id as string,
    },
    revalidate: 10,
  };
};

export const getStaticPaths: GetStaticPaths = () => {
  return {
    paths: new Array(10)
      .fill(0)
      .map((_, index) => ({ params: { id: `${index}` } })),
    fallback: true,
  };
};
