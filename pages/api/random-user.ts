// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { getRandomUser } from "../../lib/helper/getRandomUser";

export type Data = {
  name: string;
};

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  const user = getRandomUser();

  res.status(200).json(user);
}
