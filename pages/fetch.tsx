import { NextPage } from "next";
import { useEffect } from "react";

import { useUser } from "../hooks";
import Layout from "./layout";

interface Props {}

const Fetch: NextPage<Props> = () => {
  const { user, loading, error } = useUser();

  useEffect(() => {
    console.log("user effect", user);
  }, [user]);

  if (loading) {
    return <p>Loading...</p>;
  }

  if (error) {
    return <p>Error!</p>;
  }

  return (
    <Layout>
      <h1>CSR</h1>
      {user?.name}
    </Layout>
  );
};

export default Fetch;
