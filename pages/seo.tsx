import { NextPage } from "next";
import Head from "next/head";

const Seo: NextPage = () => {
  return (
    <div>
      <Head>
        <title>Test SEO</title>
        <meta name="description" content="Checkout our cool page" key="desc" />
        <meta property="og:title" content="Social Title for Cool Page" />
        <meta
          property="og:description"
          content="And a social description for our cool page"
        />
        <meta property="og:image" content="/images/shark.jpg" />
      </Head>

      <main>
        <h1>Test SEO</h1>
        <div>Try to copy url and paste on messenger.</div>
      </main>
    </div>
  );
};

export default Seo;
