import axios from "axios";
import { useQuery } from "react-query";

import { Data } from "../pages/api/hello";

interface UseUserType {
  user?: Data | null;
  error?: unknown;
  loading: boolean;
}

const useUser = (): UseUserType => {
  const { data, error, isLoading } = useQuery<Data>("/api/user", () =>
    axios.get("/api/random-user").then((res) => res.data)
  );

  if (isLoading) {
    return { user: null, error: null, loading: true };
  }

  if (error) {
    return { error, user: null, loading: false };
  }

  return { user: data, loading: false, error: null };
};

export default useUser;
