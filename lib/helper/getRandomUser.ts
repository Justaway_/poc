import { Data } from "../../pages/api/hello";

export const getRandomUser = (): Data => {
  const randomNumber = Math.floor(Math.random() * 10);

  return { name: `John Doe No.${randomNumber}` };
};
