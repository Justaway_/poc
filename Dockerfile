FROM node:14.17.6-alpine AS builder
WORKDIR /app
COPY . .
RUN yarn install 
RUN yarn build
FROM node:16-alpine AS runner
WORKDIR /app
COPY --from=builder /app/public ./public
COPY --from=builder /app/.next/standalone ./
COPY --from=builder /app/.next/static ./.next/static
EXPOSE 3000
CMD ["node", "server.js"]
